#!/bin/bash
echo "- The Caulifinder execution environment is set"
export REPET_HOST="localhost"
export REPET_USER="root"
export REPET_PW=""
export REPET_DB="test"
export REPET_PORT="3306"
export REPET_JOBS="MySQL"
export REPET_JOB_MANAGER="slurm"
export REPET_QUEUE="slurm"
export REPET_PATH=/usr/local/REPET_linux-x64-2.5
export CAULIFINDER_PATH=/usr/local/event_caulifinder
export RM_PATH=/usr/local/RepeatMasker/
export SMART_PATH=/usr/local/smart/
export PATH=$REPET_PATH/bin:$CAULIFINDER_PATH:$SMART_PATH:$RM_PATH:$PATH
export PYTHONPATH=$REPET_PATH:$CAULIFINDER_PATH:$SMART_PATH
#env
sed -i 's/<REPET_HOST>/'"$REPET_HOST"'/' $CAULIFINDER_PATH/TEdenovo.cfg
sed -i 's/<REPET_USER>/'"$REPET_USER"'/' $CAULIFINDER_PATH/TEdenovo.cfg
sed -i 's/<REPET_PW>/'"$REPET_PW"'/' $CAULIFINDER_PATH/TEdenovo.cfg
sed -i 's/<REPET_DB>/'"$REPET_DB"'/' $CAULIFINDER_PATH/TEdenovo.cfg
sed -i 's/<REPET_PORT>/'"$REPET_PORT"'/' $CAULIFINDER_PATH/TEdenovo.cfg
sed -i 's/<your_project_name>/subgenome/' $CAULIFINDER_PATH/TEdenovo.cfg
sed -i "s/<absolute_path_to_your_project_directory>/\/home\/centos/" $CAULIFINDER_PATH/TEdenovo.cfg

cp $CAULIFINDER_PATH/TEdenovo.cfg /home/centos/.
chown centos:centos /home/centos/TEdenovo.cfg
cp $CAULIFINDER_PATH/forCDD_database.sh /home/centos/.
chown centos:centos /home/centos/forCDD_database.sh
chmod 755 /home/centos/forCDD_database.sh

echo "- slurm resources adaptation to nb CPUs of docker minus 1"
nbCpus=$(($(cat /proc/cpuinfo| grep -c processor)-1))
cd /etc/slurm
mv slurm.conf slurm.conf_orig
#delete the 7 last lines
head --line=-7 slurm.conf_orig > slurm.conf
# 3 new lines added
echo "NodeName=node[1-$nbCpus] NodeHostName=localhost NodeAddr=127.0.0.1 RealMemory=1000 State=UNKNOWN" >> slurm.conf
echo "# PARTITIONS" >> slurm.conf
echo "PartitionName=normal.q Default=yes Nodes=ALL Priority=50 DefMemPerCPU=500 Shared=YES MaxNodes=$nbCpus MaxTime=5-00:00:00 DefaultTime=5-00:00:00 State=UP" >> slurm.conf
supervisorctl stop slurmctld
supervisorctl stop slurmd
supervisorctl start slurmd
supervisorctl start slurmctld
cd
echo "\tThe 'normal.q' queue will use $nbCpus CPUs"

echo "- Inside centos user home"
cd /home/centos
echo 'PS1="$PWD> "' > .bashrc
{ echo "ln -s -f /usr/local/event_caulifinder/banks/Caulimoviridae_ref_genomes.fa .";
echo "ln -s -f /usr/local/event_caulifinder/banks/baits.fa .";
echo "ln -s -f /usr/local/event_caulifinder/banks/Caulimo_RT_probes.fa .";
echo "ln -s -f /usr/local/event_caulifinder/banks/Tree_RT_set.fa .";
} >> .bashrc
chown centos:centos /home/centos/.bashrc
su centos
