FROM giovtorres/docker-centos7-slurm:17.02.9

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
ENV PATH "/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin"

# Update system packages & install sudo & unzip
# Install common YUM dependency packages
RUN set -ex \
    && yum makecache fast \
    && yum -y update \
    && yum -y install \
        curl \
        cpan \
        cppunit-devel \
        cppunit \
        expect \
        gedit. \
        MySQL-python \
        python27-PyYAML \
        python2-biopython \
        python-pip \
        rng-tools \
        unzip \
        perl-CPAN \
        perl-App-cpanminus \
    && yum clean all \
    && rm -rf /var/cache/yum

WORKDIR /tmp
RUN mkdir packages
COPY packages/blast2.linux26-x64.tar.gz packages/.
COPY packages/piler-2017_03_08-stable.x86_64.rpm packages/.
COPY packages/uclustq1.2.22_i86linux64.tar.gz packages/.
COPY packages/event_caulifinder-master.tar.gz packages/.
COPY packages/smart.tar.gz packages/.
WORKDIR /tmp/packages
RUN wget -q http://genometools.org/pub/genometools-1.5.8.tar.gz \
    && tar -zxf genometools-1.5.8.tar.gz \
    && cd genometools-1.5.8 \
    && make -s cairo=no install \
    && cd /usr/local \
    && rm -rf genometools-1.5.8 genometools-1.5.8.tar.gz \
    && wget -q http://tandem.bu.edu/trf/downloads/trf404.linux64 \
    && cp trf404.linux64 bin/trf \
    && chmod 755 bin/trf \
    && rm -f trf404.linux64 \
    && cd /usr/local \
    && wget -q http://www.repeatmasker.org/rmblast-2.11.0+-x64-linux.tar.gz \
    && mkdir rmblast \
    && tar zxvf rmblast-2.11.0+-x64-linux.tar.gz \
    && cp -r  rmblast-2.11.0/* rmblast/   


WORKDIR /usr/local
RUN wget -q http://repeatmasker.org/RepeatMasker-open-4-0-9-p2.tar.gz \
    && tar -zxf RepeatMasker-open-4-0-9-p2.tar.gz \
    && rm -rf RepeatMasker-open-4-0-9-p2.tar.gz \
    && cpanm Text::Soundex

WORKDIR /usr/local/RepeatMasker
RUN perl ./configure -trfbin=/usr/local/bin/trf -rmblastbin=/usr/local/rmblast


WORKDIR /usr/local
RUN wget -q https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.11.0/ncbi-blast-2.11.0+-x64-linux.tar.gz \
    && tar -zxf ncbi-blast-2.11.0+-x64-linux.tar.gz \
    && cp ncbi-blast-2.11.0+/bin/* /usr/local/bin/. \
    && rm -rf ncbi-blast-2.11.0+ ncbi-blast-2.11.0+-x64-linux.tar.gz \
    && wget -q https://ftp.ncbi.nlm.nih.gov/blast/executables/legacy.NOTSUPPORTED/2.2.26/blast-2.2.26-x64-linux.tar.gz \
    && tar -zxf blast-2.2.26-x64-linux.tar.gz \
    && cp blast-2.2.26/bin/* /usr/local/bin/. \
    && rm -rf blast-2.2.26 blast-2.2.26-x64-linux.tar.gz

WORKDIR /usr/local
RUN wget -q  http://www.atgc-montpellier.fr/download/binaries/phyml/PhyML-3.1.zip \
    && unzip PhyML-3.1.zip \
    && cp PhyML-3.1/PhyML-3.1_linux64 bin/phyml \
    && chmod 755 bin/phyml \
    && rm -r PhyML-3.1 PhyML-3.1.zip \
    && wget -q http://molevol.cmima.csic.es/castresana/Gblocks/Gblocks_Linux64_0.91b.tar.Z \
    && tar -zxf Gblocks_Linux64_0.91b.tar.Z \
    && cp Gblocks_0.91b/Gblocks /usr/local/bin/. \
    && rm -rf  Gblocks_0.91b Gblocks_Linux64_0.91b.tar.Z \
    && wget -q https://github.com/npchar/Phylogenomic/archive/refs/heads/master.zip \
    && unzip master.zip \
    && cp Phylogenomic-master/fasta2relaxedPhylip.pl /usr/local/bin/. \
    && rm -rf Phylogenomic-master master.zip \
    && wget -q http://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_i86linux64.tar.gz \
    && tar -zxf muscle3.8.31_i86linux64.tar.gz \
    && cp muscle3.8.31_i86linux64 /usr/local/bin/muscle \
    && rm -rf muscle3.8.31_i86linux64 muscle3.8.31_i86linux64.tar.gz \
    && cp /tmp/packages/uclustq1.2.22_i86linux64.tar.gz . \
    && tar -zxf uclustq1.2.22_i86linux64.tar.gz \
    && cp uclustq1.2.22_i86linux64 /usr/local/bin/uclust \
    && rm -rf uclustq1.2.22_i86linux64 uclustq1.2.22_i86linux64.tar.gz

WORKDIR /usr/local
RUN rpm -i --prefix=/usr/local/bin /tmp/packages/piler-2017_03_08-stable.x86_64.rpm \
    && mv /usr/local/bin/piler2 /usr/local/bin/piler

WORKDIR /usr/local
RUN wget -q http://www.repeatmasker.org/RepeatModeler/RECON-1.08.tar.gz \
    && tar -zxf RECON-1.08.tar.gz \
    && cd RECON-1.08/src \
    && make -s \
    && sed -i.bak 's/BINDIR  = ..\/bin/BINDIR  = \/usr\/local\/bin/' Makefile \
    && make install \
    && cd ../scripts \
    && sed -i.back -r 's:path = "":path = "/usr/local/bin":' recon.pl \
    && cp recon.pl /usr/local/bin/. \
    && cd /usr/local \
    && rm -rf RECON-1.08 RECON-1.08.tar.gz \
    && wget -q https://github.com/arq5x/bedtools2/archive/v2.26.0.tar.gz \
    && tar -xzf v2.26.0.tar.gz \
    && cd bedtools2-2.26.0 \
    && make -s \
    && make install \
    && cd /usr/local \
    && rm -rf bedtools2-2.26.0 v2.26.0.tar.gz \
    && export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH \
    && wget -q https://github.com/agordon/libgtextutils/releases/download/0.7/libgtextutils-0.7.tar.gz \
    && tar -zxf libgtextutils-0.7.tar.gz \
    && cd libgtextutils-0.7 \
    && export PKG_CONFIG_PATH=/tmp/packages/libgtextutils-0.7:$PKG_CONFIG_PATH \
    && ./configure \
    && make -s \
    && make install \
    && ln -s /usr/local/lib/libgtextutils-0.7.so.0.0.0 /lib64/libgtextutils-0.7.so.0 \
    && cd /usr/local \
    && rm -rf libgtextutils-0.7 libgtextutils-0.7.tar.gz \
    && wget -q https://github.com/agordon/fastx_toolkit/releases/download/0.0.14/fastx_toolkit-0.0.14.tar.bz2 \
    && tar -xjf fastx_toolkit-0.0.14.tar.bz2 \
    && cd fastx_toolkit-0.0.14 \
    && ./configure \
    && make -s \
    && make install \
    && cd /usr/local \
    && rm -rf fastx_toolkit-0.0.14 fastx_toolkit-0.0.14.tar.bz2 \
    && wget -q http://debian.rub.de/ubuntu/pool/universe/e/emboss/emboss_6.6.0.orig.tar.gz \
    && tar -zxf emboss_6.6.0.orig.tar.gz \
    && cd EMBOSS-6.6.0 \
    && export CC=/usr/bin/cc \
    && ./configure --without-x --disable-shared \
    && make -s install \
    && cd /usr/local \
    && rm -rf EMBOSS-6.6.0 emboss_6.6.0.orig.tar.gz \
    && wget -q http://eddylab.org/software/squid/squid.tar.gz \
    && tar -zxf squid.tar.gz \
    && cd squid-1.9g \
    && ./configure \
    && make -s \
    && make install \
    && cd /usr/local \
    && rm -rf squid-1.9g squid.tar.gz \
    && wget -q https://github.com/scapella/trimal/archive/trimAl.zip \
    && unzip trimAl.zip \
    && cd trimal-trimAl/source \
    && make -s \
    && cp readal /usr/local/bin/. \
    && cp trimal /usr/local/bin/. \
    && cp statal /usr/local/bin/. \
    && cd /usr/local \
    && rm -rf trimal-trimAl trimAl.zip

## Create centos user
RUN adduser centos \
    && echo "centos:centos" | chpasswd

## Install S-MART, REPET v2.5 and Caulifinder
COPY setEnv.sh /tmp/.
WORKDIR /usr/local
RUN wget -q https://urgi.versailles.inra.fr/download/repet/REPET_linux-x64-2.5.tar.gz \
    && tar -zxf REPET_linux-x64-2.5.tar.gz \
    && chmod -R 755 REPET_linux-x64-2.5 \
    && sed -i '296d' REPET_linux-x64-2.5/commons/tools/LaunchGrouperInParallel.py \
    && rm REPET_linux-x64-2.5.tar.gz \
    && ln -s /usr/lib64/libcppunit-1.12.so.1.0.0 /usr/lib64/libcppunit-1.12.so.0 \
    && tar -zxf /tmp/packages/event_caulifinder-master.tar.gz \
    && mv event_caulifinder-master event_caulifinder \
    && chmod -R 755 event_caulifinder \
    && chown centos:centos event_caulifinder \
    && tar -zxf /tmp/packages/smart.tar.gz \
    && chmod -R 755 smart \
    && chown centos:centos event_caulifinder

CMD ["bash", "/tmp/setEnv.sh"]
